#!/usr/bin/env bash
export RPZ='basic.rpz'
export HOSTS127='1.0.0.127.hosts'
export HOSTS0='0.0.0.0.hosts'
export DOMAINLIST='domainonly'

export ZONE='rpz.mypdns.cloud'
export PDNSUTIL=`(which pdnsutil)`
export PHP=`(which php)`
export WGET=`(which wget)`
export CURL=`(which curl)`

WORKDIR=$(pwd)/

rm -f $DOMAINLIST $HOSTS0 $HOSTS127 $RPZ

CURLTEMP=$(mktemp /tmp/curl_temp.XXXXXXXXXX) || { echo "Failed to create temp file"; exit 1; }
chmod +x {$curltemp,,}
trap "rm -f $curltemp" 0 2 3 15

OUT=$(mktemp /tmp/output.XXXXXXXXXX) || { echo "Failed to create temp file"; exit 1; }
chmod +x $OUT
trap "rm -f $OUT" 0 2 3 15

sort_temp=$(mktemp /tmp/sort_temp.XXXXXXXXXX) || { echo "Failed to create temp file"; exit 1; }
chmod +x $sort_temp
trap "rm -f $sort_temp" 0 2 3 15

echo 'Downloading your lists'
	${CURL} -sS -L --compressed "https://github.com/FadeMind/hosts.extras/raw/master/hpHosts/hosts" | grep -v '^#' | grep -v '^-' | grep -v 'localhost' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://github.com/FadeMind/hosts.extras/raw/master/rlwpx.free.fr.hrsk/hosts" | grep -v '^#' | grep -v '^-' | grep -v 'localhost' | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt" | sed -e 's/#.*$//g' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hostsfile.mine.nu/HOSTS0.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d' |cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/ad_servers.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/download/hosts.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/emd.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/exp.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/fsa.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/grm.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/hjk.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/mmt.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/psh.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hosts-file.net/pup.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost$/d' | cut -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://hostsfile.org/Downloads/hosts.txt" | sed -e 's/#.*$//g; s/^ *//; s/ *$//; /^$/d; /^\s*$/d; /localhost/d' | cut -f2 >> {$CURLTEMP,,}

	${CURL} -sS -L --compressed "https://mirror.cedia.org.ec/malwaredomains/immortal_domains.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=0" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://phishing.army/download/phishing_army_blocklist_extended.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://ransomwaretracker.abuse.ch/downloads/CW_C2_DOMBL.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://ransomwaretracker.abuse.ch/downloads/LY_C2_DOMBL.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://ransomwaretracker.abuse.ch/downloads/TC_C2_DOMBL.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://ransomwaretracker.abuse.ch/downloads/TL_C2_DOMBL.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt" | sed -e 's/#.*$//g; /^\s*$/d' | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/anudeepND/blacklist/master/CoinMiner.txt" | sed -e 's/#.*$//g; /^\s*$/d' | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/anudeepND/blacklist/master/facebook.txt" | sed -e 's/#.*$//g; /^\s*$/d' | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/anudeepND/youtubeadsblacklist/master/domainlist.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt" | sed -e 's/#.*$//g; /^\s*$/d' | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/Dawsey21/Lists/master/main-blacklist.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/HorusTeknoloji/TR-PhishingList/master/url-lists.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/matomo-org/referrer-spam-blacklist/master/spammers.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/android-tracking.txt" | sed -e 's/#.*$//g; /^\s*$/d' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt" | sed -e 's/#.*$//g; /^\s*$/d; s/\/\///g' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.2o7Net/hosts" | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts" | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Spam/hosts" | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/StevenBlack/hosts/master/data/KADhosts/hosts" | sed -e 's/#.*$//g; /^\s*$/d; s/\/\///g' | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts" | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts" | grep -v 'localhost' | grep -v 'ip6-' | grep -v local | grep -v broadcasthost| sed -e 's/#.*$//g; /^\s*$/d; s/\/\///g' | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://raw.githubusercontent.com/vokins/yhosts/master/hosts" | grep -v '#' | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://reddestdream.github.io/Projects/MinimalHosts/etc/MinimalHostsBlocker/minimalhosts" | grep -v '::' | grep -v '#' | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://s3.amazonaws.com/lists.disconnect.me/simple_malvertising.txt" | grep -v '#' | grep -v 'Malvertising list by Disconnect' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt" | grep -v '#' | grep -v 'Malvertising list by Disconnect' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://someonewhocares.org/hosts/zero/hosts" | grep -v '#' | grep -v '127.0.0.1' | grep -v '::' | grep -v 'broadcasthost'  | cut -d' ' -f2 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/AdAway.txt" | grep -v 'localhost' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/AdguardDNS.txt" | grep -v 'localhost' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Airelle-trc.txt" | grep -v 'localhost' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Cameleon.txt" | grep -v 'localhost' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Disconnect-ads.txt" | grep -v 'localhost' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Disconnect-mal.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Disconnect-trc.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Dshield-Sus.txt" | grep -v '^site$' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Easylist-Dutch.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Easylist.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Easyprivacy.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/HostsFileOrg.txt" | grep -v '^localhost$' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/HPHosts-ads.txt" | grep -v '^localhost$' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/JoeWein.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/JoeyLane.txt" | grep -v '^localhost$' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Kowabit.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/PeterLowe.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/PiwikSpam.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Prigent-Ads.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Quidsup-trc.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/ReddestDream.txt" | grep -v 'localhost' | grep -v 'broadcasthost' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/SB2o7Net.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/SBKAD.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/SBSpam.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/SBUnchecky.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/SomeoneWC.txt" | grep -v 'localhost' | grep -v 'localhost.localdomain' | grep -v 'broadcasthost' | grep -v '^local$' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Spam404.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Vokins.txt" >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://v.firebog.net/hosts/Winhelp2002.txt" | grep -v '^localhost$' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.dshield.org/feeds/suspiciousdomains_High.txt" | grep -iv '^site$' | grep -v '#' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.dshield.org/feeds/suspiciousdomains_Low.txt" | grep -iv '^site$' | grep -v '#' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.dshield.org/feeds/suspiciousdomains_Medium.txt" | grep -iv '^site$' | grep -v '#' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.joewein.net/dl/bl/dom-bl-base.txt" | cut -d';' -f1 | grep -v '#' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.malwaredomainlist.com/hostslist/hosts.txt" | dos2unix | grep -v '#' | grep -iv 'localhost' | cut -d' ' -d' ' -f3 | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.squidblacklist.org/downloads/dg-ads.acl" | grep -v '#' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://www.squidblacklist.org/downloads/dg-malicious.acl" | grep -v '#' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "http://sysctl.org/cameleon/hosts" | grep -v '#' | grep -vi 'localhost' | cut -f2 | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://zerodot1.gitlab.io/CoinBlockerLists/hosts" | grep -v '#' | cut -d' ' -f2 >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://zeustracker.abuse.ch/blocklist.php?download=baddomains" | grep -v '#' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist" | grep -v '#' | awk '/[a-z]$/{print $0}' >> {$CURLTEMP,,}
	${CURL} -sS -L --compressed "http://winhelp2002.mvps.org/hosts.txt" | dos2unix | grep -v '#' | grep -vi 'localhost' | awk '/[a-z]$/{print $0}' | cut -d' ' -f2 >> {$CURLTEMP,,}
	
	
	### several dublicated lines here
	${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/add.Risk/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/add.Spam/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/antipopads/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/blocklists-facebook/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/StreamingAds/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/UncheckyAds/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- 'https://urlhaus.abuse.ch/downloads/rpz/' | dos2unix | grep -i '^[a-z0-9_]' | cut -d' ' -f 1 | sort -u >> {$CURLTEMP,,}
	
	${WGET} -qO- "https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/hosts?inline=false" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
	${WGET} -qO- "https://raw.githubusercontent.com/jawz101/MobileAdTrackers/master/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://lists.malwarepatrol.net/cgi/getfile?receipt=250079247602&product=34&list=hosts_macos_agressive" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://raw.githubusercontent.com/anudeepND/youtubeadsblacklist/master/domainlist.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://hosts-file.net/ad_servers.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=0" >> {$CURLTEMP,,}
##${CURL} -sS -l --compressed "http://www.hostsfile.org/Downloads/hosts.txt" >> {$CURLTEMP,,}
##${CURL} -sS -l --compressed "https://github.com/FadeMind/hosts.extras/raw/master/hpHosts/hosts" >> {$CURLTEMP,,}
##${CURL} -sS -l --compressed "https://github.com/FadeMind/hosts.extras/raw/master/rlwpx.free.fr.hrsk/hosts" >> {$CURLTEMP,,}
##${CURL} -sS -l --compressed "https://github.com/FadeMind/hosts.extras/raw/master/rlwpx.free.fr.htrc/hosts" >> {$CURLTEMP,,}
##${WGET} -qO- "https://raw.githubusercontent.com/jawz101/MobileAdTrackers/master/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://gitlab.com/ZeroDot1/CoinBlockerLists/raw/master/hosts?inline=false" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/StreamingAds/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/UncheckyAds/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/add.Risk/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/add.Spam/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/antipopads/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
##${WGET} -qO- "https://github.com/FadeMind/hosts.extras/raw/master/blocklists-facebook/hosts" | cut -d ' ' -f 2 | sort -u >> {$CURLTEMP,,}
###${CURL} -sS -L --compressed "https://api.hackertarget.com/hostsearch/?q=googlevideo.com" | awk -F, 'NR>1 {print $1}' >> {$CURLTEMP,,}
###${CURL} -sS -L --compressed "https://api.hackertarget.com/hostsearch/?q=facebook.com" | awk -F, 'NR>1 {print $1}' >> {$CURLTEMP,,}
##${CURL} -sS -l --cpmpressed "https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://hosts-file.net/download/hosts.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "https://hosts-file.net/psh.txt" >> {$CURLTEMP,,}
##${CURL} -sS -L --compressed "http://winhelp2002.mvps.org/hosts.txt" >> {$CURLTEMP,,}
#${CURL} -sS -L --compressed "" >> {$CURLTEMP,,}

#echo ''
#echo 'Add our own domain list and lower-case output'

#	cat justdomains >> {$CURLTEMP,,}

#cp -v "$filename" ./"${filename}".tmp
sed -i -e 's/#.*$//g' -e 's/;.*$//g' "$CURLTEMP" ## -e 's/!.*$//g' -e '/^\s*$/d' -e 's/^-.*$//g' -e 's/127.0.0.1//' -e 's/0.0.0.0//g' -e 's/CNAME//' -e 's/[[:blank:]]\+/ /g' -e 's/^github.com//' -e 's/^github.io//' -e 's/^bitbucket.org//' -e 's/^bitbucket.io//' -e 's/^raw.githubusercontent.com//' -e 's/^s3.amazonaws.com//' -e 's/^localhost//' -e 's/^:://' -e 's/\\013//' -e 's/files.pythonhosted.org//' -e 's/pythonhosted.org//' "$CURLTEMP"
#sed -i -e 's/\r//g' "$CURLTEMP"

# Delete all lines that do not have a dot within it's line
#sed -i -e '/^.*\..*$/!d' "$CURLTEMP"

printf '\nCount lines in curltemp\n'

	wc -l $CURLTEMP

printf '\n\n\nwrite the temp to file and sort it with uniq, Make sure the file is utf-8, ensure lower-case\n\n'

	sort $CURLTEMP | uniq -u > $sort_temp


printf '\nCount lines in sort_temp\n'

wc -l $sort_temp

printf '\nWe need to convert the output in seperate process\n'

	iconv -f ISO-8859-1 -t UTF-8 $sort_temp > {$DOMAINLIST,,}

printf '\n\n'

	#sort $CURLTEMP  | uniq -u >> $sort_temp

	#exec 4<$sort_temp
	#echo Start
	#while read -r -u4 nxdomain ; do
	    #echo "$PDNSUTIL add-record $ZONE $nxdomain CNAME 86400 ." >> $OUT
	    #echo "PDNSUTIL add-record YOUR.RPZ.ZONE $nxdomain CNAME 86400 ." >> rpz.mypdns.cloud
	    #echo "127.0.0.1 $nxdomain" >> {$HOSTS127,,}
	    #echo "0.0.0.0 $nxdomain" >> {$HOSTS0,,}
	    #echo "$nxdomain" >> {$DOMAINLIST,,}
	#done < "$sort_temp"

	#$OUT

	##$PHP pdns2bind.php
	##mv ./tmp/rpz.mypdns.cloud.zone ./$RPZ
	##rm -fr ./tmp/


	#echo ''
	#echo ''
	#ls -lh $OUT $RPZ $HOSTS127 $HOSTS0 $domainlist
	#echo ''
	#echo ''
	#wc -l $OUT $RPZ $HOSTS127 $HOSTS0 $domainlist

printf '\n\n'
head $DOMAINLIST

printf '\nLets work with pyfunceble\n\n'

##head -n 40 $RPZ
#$PDNSUTIL rectify-zone $ZONE
#$PDNSUTIL increase-serial $ZONE
#$PDNSUTIL show-zone $ZONE

pyfunceble --syntax -nw --plain --idna -f $WORKDIR/domainonly

echo ''
echo ''
echo 'count accepted domains'
echo ''
echo ''
wc -l $WORKDIR/output/domains/VALID/list
echo ''
echo ''

echo ''
echo 'Lets move the valid domains to domainonly'

	sort $WORKDIR/output/domains/VALID/list | uniq -u > $WORKDIR/domainonly

echo ''
echo 'Clean up'

rm -f $sort_temp $CURLTEMP
exit ${?}