-- Copyright (C) <2018 -> > <My privacy DNS>
--
-- This program is free software: you can redistribute it and/or modify it under
-- the terms of our modified GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
-- for more details.
--
-- You should have received a copy of the MODIFIED GNU Affero General Public License
-- along with this program. If not, see ​https://www.mypdns.org/trac/wiki/License


for add-record in io.lines("/home/joakim/BitBucket/Team/mypdns/abuse-list/mbl.txt") do
    (/usr/bin/pdnsutil add-record rpz.mypdns.cloud.zone (add-record))
end